/// <reference types="Cypress" />

context('Polling', () => {
  beforeEach(() => {
    // https://stackoverflow.com/questions/50820732/in-cypress-set-a-token-in-localstorage-before-test
    cy.request('POST', 'http://35.247.135.93.xip.io:81/v1/staff/login', {
      LoginForm: {
        username: 'staffprov',
        password: '123456'
      }
    }).then((resp) => {
      cy.setCookie('Admin-Token', resp.body.data.access_token)
    })
  })

  it('Staff Provinsi can lihat daftar Polling', () => {
    cy.visit('/#/polling/index');

    cy.get('#breadcrumb-container').should('contain', 'Daftar Polling');

    cy.get('.app-main')
      .should('contain', 'Nama Polling')
      .should('contain', 'Mulai');
  })

  it('Tombol Tambah Polling', () => {
    cy.visit('/#/polling/index');

    cy.contains('Tambah Polling Baru').click();

    cy.get('#breadcrumb-container').should('contain', 'Tambah Polling');
  })

  it('Lihat daftar Polling', () => {
    cy.visit('http://35.247.135.93:8081/#/polling/index');

    cy.get('#breadcrumb-container').should('contain', 'Daftar Polling');

    cy.get('.app-main')
      .should('contain', 'Nama Polling')
      .should('contain', 'Mulai');
  })

  it('Form Tambah Polling', () => {
    cy.visit('/#/polling/create');

    cy.get('#breadcrumb-container').should('contain', 'Tambah Polling');

    cy.get('label').contains('Nama Polling').next('div').find('input')
      .type('Test Polling Baru').should('have.value', 'Test Polling Baru');

    cy.get('label').contains('Kategori').next('div').find('.el-select')
      .click();

    cy.get('.el-scrollbar').contains('Ekonomi').click();

    cy.get('label').contains('Deskripsi').next('div').find('textarea')
      .type('Test Deskripsi Polling Baru').should('have.value', 'Test Deskripsi Polling Baru');
    cy.contains('Simpan sebagai Draft').click();

    cy.get('label').contains('Pengantar').next('div').find('textarea').should('have.css', 'border-color', 'rgb(255, 73, 73)');
    cy.get('label').contains('Pengantar').next('div').should('contain', 'Pengantar harus diisi');
  })
})