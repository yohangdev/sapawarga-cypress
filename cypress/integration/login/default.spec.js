/// <reference types="Cypress" />

context('Login as Staff Provinsi', () => {
  it('Staff Provinsi cannot login with wrong password', () => {
    cy.visit('/');

    cy.get('input[name="username"]')
      .type('staffprov').should('have.value', 'staffprov');

    cy.get('input[name="password"]')
      .type('wrongpassword').should('have.value', 'wrongpassword');

    cy.get('button').click();

    cy.wait(1000);

    cy.get('body').should('contain', 'Username atau kata sandi salah.');
  })

  it('Staff Provinsi cannot login with empty password', () => {
    cy.visit('/');

    cy.get('input[name="username"]')
      .type('staffprov').should('have.value', 'staffprov');

    cy.get('button').click();

    cy.get('body').should('contain', 'Kata sandi harus diisi');
  })

  it('Staff Provinsi cannot login with empty username', () => {
    cy.visit('/');

    cy.get('input[name="password"]')
      .type('wrongpassword').should('have.value', 'wrongpassword');

    cy.get('button').click();

    cy.get('body').should('contain', 'Username harus diisi');
  })

  it('Staff Provinsi cannot login with empty username and password', () => {
    cy.visit('/');

    cy.get('button').click();

    cy.get('body').should('contain', 'Username harus diisi');
    cy.get('body').should('contain', 'Kata sandi harus diisi');
  })

  it('Staff Provinsi can login into Web Admin', () => {
    cy.visit('/');

    cy.get('input[name="username"]')
      .type('staffprov').should('have.value', 'staffprov');

    cy.get('input[name="password"]')
      .type('123456').should('have.value', '123456');

    cy.get('button').click();

    cy.get('#app').should('contain', 'Admin staff Provinsi Jawa Barat');
  })
})